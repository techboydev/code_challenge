﻿using System;
using Agl.Models.Pets;
using Xunit;

namespace Agl.Unit.Test
{
    /// <summary>
    /// Testing the constructor of pet related models
    /// </summary>
    public class PetModelTest
    {
        /// <summary>
        /// Test constructor for pet model
        /// </summary>
        [Fact]
        public void Constructor_Pet_Created()
        {
            //arrange
            var myPet = new Pet();
            //act
            myPet.Name = "Garfield";
            myPet.Type = "Dog";
            //asssert the result
            Assert.False(myPet.Type == "Cat");
            Assert.True(myPet.Name=="Garfield");
        }
        /// <summary>
        /// Test CatResponse constructor with test gender data.
        /// </summary>
        /// <param name="gender"></param>
        [Theory]
        [InlineData("Male")]
        [InlineData("Female")]
        public void Constructor_CatResponse_Created(string gender)
        {
            //arrange
            var myPet = new CatResponse();
            //act
            myPet.Gender = gender;
            myPet.Cats = new Pet[] { };
            Assert.NotNull(myPet.Gender);

        }
        /// <summary>
        /// Owner constructor test created
        /// </summary>
        [Fact]
        public void Constructor_Owner_Created()
        {
            //arrange
            var owner = new Owner();
            //act
            owner.Age = 15;
            owner.Name = "Jen";
            owner.Gender = "Male";
            owner.Pets = new System.Collections.Generic.List<Pet>();
            //asssert the result
            Assert.False(owner == null);
            Assert.True(owner.Pets.Count == 0);
        }
        /// <summary>
        /// Constructor testing on owner model created with different gender values
        /// </summary>
        /// <param name="gender">Gender of owner</param>
        [Theory]
        [InlineData("Male")]
        [InlineData("Female")]
        public void Constructor_OwnerGender_ValidGender(string gender)
        {
            //arrange
            var owner = new Owner();
            //act
            owner.Gender = gender;
            owner.Name = "Jen";
            owner.Pets = new System.Collections.Generic.List<Pet>();
            //asssert the result
            Assert.True(owner.Gender == "Male" || owner.Gender == "Female");
        }
    }
}
