using System;
using System.Linq;
using Xunit;
using Agl.Models.Interface;
using Moq;
using Agl.Controllers;
using Agl.Models.Pets;

namespace Agl.Test
{
    /// <summary>
    /// Pet Controller Test class
    /// </summary>
    public class PetControllerUnitTest
    {
        /// <summary>
        /// Mock data response
        /// </summary>
        public CatResponse[] mockData { get; set; } = new Models.Pets.CatResponse[]
            {
                  new Models.Pets.CatResponse
                  {
                      Gender="Male",
                      Cats= new Models.Pets.Pet[]
                      {
                          new Models.Pets.Pet
                          {
                              Name="Aoo",Type="Cat"
                          }
                       }
                  },
                  new Models.Pets.CatResponse
                  {
                      Gender="Female",
                      Cats= new Models.Pets.Pet[]
                      {
                          new Models.Pets.Pet
                          {
                              Name="Gerkin",Type="Dog"
                          }
                       }
                  }
             };
        /// <summary>
        /// Testing number of cat return in the set for false case.
        /// </summary>
        [Fact]
        public void GetAllCat_CountReturnCats_ShouldBeInvalid()
        {
            //arrange
            var petService = new Mock<IPetService>();

            var petController = new PetController(petService.Object);
           
            petService
               .Setup(p => p.GetAllCats())
               .Returns(mockData);
            var result = petController.Get()
                .Any(x => x.Gender == "Male" && x.Cats.Count() > 1);
                
            //asssert the result
            Assert.False(result);
        }
        /// <summary>
        /// Unit testing GetAllCat method from Pet controller level.
        /// </summary>
        [Fact]
        public void GetAllCat_ExistEndPoint_ShouldBeValid()
        {
            //arrange
            var petService = new Mock<IPetService>();

            petService
                .Setup(p => p.GetAllCats())
                .Returns(mockData);
            var petController = new PetController(petService.Object);
            //act
            var result = petController.Get();
            //asssert the result
            Assert.NotNull(result);
        }
        /// <summary>
        /// Testing whether the responses array should be 0 if there is no mock.
        /// </summary>
        [Fact]
        public void GetAllCat_EmptyArray_ShouldBeEmptyResult()
        {
            //arrange
            var petService = new Mock<IPetService>();

            var petController = new PetController(petService.Object);
            //act
            var result = petController.Get();
            //asssert the result
            Assert.True(result.Length==0);
        }
        

    }
}
