﻿using System;
using System.Linq;
using Microsoft.Extensions.Configuration;
using Xunit;
using Agl.Models.Interface;
using System.Net.Http;
using Moq;
using Agl.Models.DataProivder;
using Agl.Controllers;
using Agl.Models.Pets;
using Agl.Models.Services;

namespace Agl.Unit.Test
{
    public class PetServiceTest
    {
        /// <summary>
        /// Mock responses to be returned in the test
        /// </summary>
        public CatResponse[] mockData { get; set; } = new Models.Pets.CatResponse[]
            {
                  new Models.Pets.CatResponse
                  {
                      Gender="Male",
                      Cats= new Models.Pets.Pet[]
                      {
                          new Models.Pets.Pet
                          {
                              Name="Aoo",Type="Cat"
                          }
                       }
                  },
                  new Models.Pets.CatResponse
                  {
                      Gender="Female",
                      Cats= new Models.Pets.Pet[]
                      {
                          new Models.Pets.Pet
                          {
                              Name="Gerkin",Type="Dog"
                          }
                       }
                  }
             };
        /// <summary>
        /// Test method to check the count on return value
        /// </summary>
        [Fact]
        public void GetAllCat_TwoResultSet_ValidResponse()
        {
            //arrange
            var dataProvider = new Mock<IDataProvider>();
            dataProvider.Setup(p => p.GetAllCats())
                .Returns(mockData);

            var petService = new PetService(dataProvider.Object);
           
            //act
            var result = petService.GetAllCats();
            //asssert the result
            Assert.True(result.Count()==2);
        }
        /// <summary>
        /// Test to make sure a set of pet for male owner exist
        /// </summary>
        [Fact]
        public void GetAllCat_MaleSet_ValidResponse()
        {
            //arrange
            var dataProvider = new Mock<IDataProvider>();
            dataProvider.Setup(p => p.GetAllCats())
                .Returns(mockData);

            var petService = new PetService(dataProvider.Object);

            //act
            var result = petService.GetAllCats();
            //asssert the result
            Assert.Contains(result,x=>x.Gender=="Male");
        }
        /// <summary>
        /// Check whether correct count of return response.
        /// </summary>
        /// <param name="count"></param>
        [Theory]
        [InlineData(1)]
        [InlineData(2)]
        public void GetAllCat_TotalInResultSet_ShouldMatchTheData(int count)
        {
            //arrange
            var dataProvider = new Mock<IDataProvider>();
            dataProvider.Setup(p => p.GetAllCats())
                .Returns(mockData);

            var petService = new PetService(dataProvider.Object);

            //act
            var result = petService.GetAllCats();
            //asssert the result
            if (count == 1)
                Assert.False(result.Count() == count);
            else
                Assert.True(result.Count() == count);
        }
        /// <summary>
        /// Check the see whether the PetService constructor created.
        /// </summary>
        [Fact]
        public void Constructor_PetService_ShouldCreateObject()
        {
            //arrange
            var dataProvider = new Mock<IDataProvider>();
            //act
            var petService = new PetService(dataProvider.Object);
            //asssert the result
            Assert.NotNull(petService);
        }

    }
}
