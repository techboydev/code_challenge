﻿using System;
using System.Linq;
using Microsoft.Extensions.Configuration;
using Xunit;
using Agl.Models.Interface;
using System.Net.Http;
using Moq;
using Agl.Models.DataProivder;
using Agl.Controllers;

namespace Agl.Unit.Test
{
    /// <summary>
    /// Data provider Test class
    /// </summary>
    public class DataProviderTest
    {
     /// <summary>
     /// Unit testing GetAllCat method from data provider layer.
     /// </summary>
        [Fact]
        public void GetAllCat_DataProviderLayer_ShouldReturnResult()
        {
            //arrange
            var service = new Mock<IDataProvider>();
            service
                .Setup(p => p.GetAllCats())
                .Returns(new Models.Pets.CatResponse[] {
                  new Models.Pets.CatResponse{
                      Gender="Male",
                      Cats= new Models.Pets.Pet[]{
                          new Models.Pets.Pet{
                              Name="Aoo",
                              Type="Cat"
                          }
                      }
                  }
                });
            //act
            var result = service.Object.GetAllCats();

            //asssert the result
            Assert.NotNull(result);
        }
        [Fact]
        public void GetAllCat_DataProviderLayer_Valid()
        {
            //arrange
            var service = new Mock<IDataProvider>();
            var mockData = new Models.Pets.CatResponse[]
            {
                  new Models.Pets.CatResponse
                  {
                      Gender="Male",
                      Cats= new Models.Pets.Pet[]
                      {
                          new Models.Pets.Pet
                          {
                              Name="Aoo",Type="Cat"
                          }
                       }
                  }
             };
      
            service
                .Setup(p => p.GetAllCats())
                .Returns(mockData);
            //act
            var result = service.Object.GetAllCats();

            //asssert the result
            Assert.Equal(mockData, result);
        }

        [Fact]
        public void Constructor_DataProviderLayer_Valid()
        {
            //arrange
      
            var httpClient = new Mock<IHttpClientFactory>();
            var configuration = new Mock<IConfiguration>();

            var dataPrd = new DataProvider(httpClient.Object, configuration.Object);
           

            //asssert the result
            Assert.NotNull(dataPrd);
        }
    }
}
