﻿using System;
using Agl.Models.Pets;
using Xunit;

namespace Agl.Unit.Test
{
    public class OwnerModelTest
    {

        [Fact]
        public void Constructor_Owner_Created()
        {
            //arrange
            var owner = new Owner();
            //act
            owner.Age = 15;
            owner.Name = "Jen";
            owner.Gender = "Male";
            owner.Pets = new System.Collections.Generic.List<Pet>();
            //asssert the result
            Assert.False(owner == null);
            Assert.True(owner.Pets.Count == 0);
        }
        [Theory]
        [InlineData("Male")]
        [InlineData("Female")]
        public void Constructor_OwnerGender_ValidGender(string gender)
        {
            //arrange
            var owner = new Owner();
            //act
            owner.Gender = gender;
            owner.Name = "Jen";
            owner.Pets = new System.Collections.Generic.List<Pet>();
            //asssert the result
            Assert.True(owner.Gender=="Male" || owner.Gender=="Female");
        }
    }
}
