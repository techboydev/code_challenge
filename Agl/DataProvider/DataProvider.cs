﻿using System.Net.Http;
using Newtonsoft.Json;
using System.Linq;
using System.Collections.Generic;
using Microsoft.Extensions.Configuration;
using Agl.Models.Interface;
using Agl.Models.Pets;

namespace Agl.Models.DataProivder
{
    /// <summary>
    /// DataProvider class implement IDataProvider
    /// </summary>
    public class DataProvider:IDataProvider
    {
        private IHttpClientFactory clientFactory;
        private IConfiguration configuration;
        /// <summary>
        /// Data Provider Constructor
        /// </summary>
        /// <param name="_clientFactory">Instance class implements IHttpClientFactory</param>
        /// <param name="_configuration">Instance class implements IConfiguration</param>
        public DataProvider(IHttpClientFactory _clientFactory, IConfiguration _configuration)
        {
            clientFactory = _clientFactory;
            configuration = _configuration;
        }
        /// <summary>
        /// Read data from given network location configured in appsetting
        /// </summary>
        /// <returns>string of data</returns>
        private string StreamData()
        {
            var http=clientFactory.CreateClient();
            var baseUri = configuration.GetValue<string>("DataSourceUri");
            var repo= configuration.GetValue<string>("Repo"); 
            return http.GetStringAsync(baseUri + repo).Result;
        }
        /// <summary>
        /// Convert a response from DataProvider.StreamData() call to Owner array object
        /// </summary>
        /// <returns>Owner array []</returns>
        private Owner[] GetAllPets()
        {
            var response = StreamData();
            if ( response== null)
                return new Owner[] { };
            return JsonConvert.DeserializeObject<Owner[]>(response);
        }
     
        /// <summary>
        /// Method that return all CatResponse array, sort by alphabet order by default.
        /// </summary>
        /// <returns>Collection of CatResponse</returns>
        public CatResponse[] GetAllCats()
        {
            var listOfPetOwner = GetAllPets();

            var petResult=listOfPetOwner
                .Where(x => x.Pets != null)
                .GroupBy(p=>p.Gender)
                .Select(c=>new CatResponse
                 {
                    Gender=c.Key,
                    Cats=c.SelectMany(e=>e.Pets).Where(k=> string.Compare(k.Type, "cat", true) == 0).OrderBy(s=>s.Name).ToArray()
                 })
                .ToArray();
            return petResult;
        }
         


    }

}
