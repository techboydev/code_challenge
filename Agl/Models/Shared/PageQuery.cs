﻿using System;
using System.Collections.Generic;
namespace Agl.Models.Shared
{
    public class PageQuery
    {
        public int PageNum { get; set; } = 1;
        public int PageSize { get; set; } = 20;
        public string Gender { get; set; } = "";
        public PageQuery()
        {
        }
    }
}
