﻿using System;
using System.Linq;
using System.Collections.Generic;

namespace Agl.Models.Shared
{
    /// <summary>
    /// Page util class extension of LINQ functions for paging purpose.
    /// </summary>
    public static class PageUtil
    {
        public static IEnumerable<T> Page<T>(this IEnumerable<T> collection, int pageSize, int page)
        {
               return collection.Skip((page-1)* pageSize).Take(pageSize);
        }
        public static IQueryable<T> Page<T>(this IQueryable<T> collection, int pageSize, int page)
        {
            return collection.Skip((page - 1) * pageSize).Take(pageSize);

        }
        
    }
}
