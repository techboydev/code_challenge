﻿using System;
using System.Collections.Generic;

namespace Agl.Models.Pets
{
    /// <summary>
    /// OWner Model, Owner details and collection of their pets.
    /// </summary>
    public class Owner
    {
        public string Name { get; set; }
        public string Gender { get; set; }
        public int Age { get; set; }
        public List<Pet> Pets { get; set; }
        public Owner()
        {
        }
    }
}
