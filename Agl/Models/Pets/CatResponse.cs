﻿using System;
namespace Agl.Models.Pets
{
    /// <summary>
    /// CatResponse class supports response format for front end.
    /// </summary>
    public class CatResponse
    {
        public string Gender { get; set; }
        public Pet[] Cats { get; set; }
        public CatResponse()
        {
        }
    }
}
