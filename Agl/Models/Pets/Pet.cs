﻿using System;
namespace Agl.Models.Pets
{
    /// <summary>
    /// Pets Model, store pet details.
    /// </summary>
    public class Pet
    {
        public string Name { get; set;}
        public string Type { get; set; }
        public Pet()
        {
        }
    }
}
