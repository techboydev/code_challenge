﻿using System;
using Agl.Models.Pets;

namespace Agl.Models.Interface
{
    /// <summary>
    /// IDataProvider act as data layer and responsible to read or write to database if there is.
    /// </summary>
    public interface IDataProvider
    { /// <summary>
      /// Return array of CatResponses, should be called by other services
      /// </summary>
      /// <returns></returns>
        CatResponse[] GetAllCats();
    }
}
