﻿using System;
using Agl.Models.Pets;

namespace Agl.Models.Interface
{
    /// <summary>
    /// IPetService interface will be use to retrieve data from data provider layer
    /// </summary>
    public interface IPetService
    {
        /// <summary>
        /// Return CatResponse array to controller call.
        /// </summary>
        /// <returns>CatResponse array</returns>
        CatResponse[] GetAllCats();
    }
}
