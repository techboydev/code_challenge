﻿using System;
using Agl.Models.Interface;
using Agl.Models.Pets;

namespace Agl.Models.Services
{
    /// <summary>
    /// PetService class implements IPetService interface for GetPets implementation 
    /// </summary>
    public class PetService:IPetService
    {
        private IDataProvider dataProvider;
        /// <summary>
        /// PetService constructor, expect instance implementing IDataProvider interface
        /// </summary>
        /// <param name="_dataProvider"></param>
        public PetService(IDataProvider _dataProvider)
        {
           dataProvider = _dataProvider;
        }
        /// <summary>
        /// Return CatResponse collection including all cats group by owner gender
        /// </summary>
        /// <returns>CatResponse collection</returns>
        public CatResponse[] GetAllCats()
        {
            return dataProvider.GetAllCats();
        }
    }
}
