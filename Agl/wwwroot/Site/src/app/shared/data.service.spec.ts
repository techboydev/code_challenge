import {
  async,
  ComponentFixture,
  TestBed,
  inject,
  fakeAsync,
  tick
} from "@angular/core/testing";
import {
  HttpClientTestingModule,
  HttpTestingController
} from "@angular/common/http/testing";
import { DataService } from "./data.service";
import { CustomMaterialModule } from "./custom.material.module";
import { PetComponent } from "../pet/pet.component";
import { of, throwError, Observable } from "rxjs";

describe("Data Service Component Testing", () => {
  //declare dependencies to be used for each test
  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [PetComponent],
      imports: [HttpClientTestingModule, CustomMaterialModule],
      providers: [DataService]
    });
  });

  it("Data Service Layer Created", inject(
    [DataService],
    (service: DataService) => {
      expect(service).toBeTruthy();
    }
  ));
  it("Data service Layer - No response", fakeAsync(() => {
    let fixture = TestBed.createComponent(PetComponent);
    let app = fixture.debugElement.componentInstance;
    let errorMock = {
      status: 500,
      error: {
        message: "Service is no response"
      }
    };
    //spy on getPet event or method, giving back the mock response
    let dataService = fixture.debugElement.injector.get(DataService);
    let spy = spyOn(dataService, "getAllCat").and.returnValue(
      throwError(errorMock)
    );
    app.ngOnInit();
    fixture.detectChanges();
    tick();
    expect(app.errorResponse.status).toBe(500);
  }));
});
