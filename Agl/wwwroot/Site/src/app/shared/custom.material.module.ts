import { NgModule } from '@angular/core';
import {MatListModule, 
        MatCardHeader, 
        MatCardTitle, 
        MatCardSubtitle, 
        MatCardAvatar, 
        MatCard, 
        MatIconModule, 
        MatDivider,
        MatGridList,
        MatGridTile
    } from '@angular/material';

@NgModule({
    declarations: 
    [
        MatCard,
        MatCardHeader, 
        MatCardTitle, 
        MatCardSubtitle, 
        MatCardAvatar,
        MatGridList,
        MatGridTile
    ],
    imports: 
    [
        MatListModule,
        MatIconModule
    ],
    exports: 
    [
        MatIconModule,
        MatListModule,
        MatCard,
        MatCardHeader, 
        MatCardTitle, 
        MatCardSubtitle, 
        MatCardAvatar,
        MatGridList,
        MatGridTile
        
    ],
})

export class CustomMaterialModule { }