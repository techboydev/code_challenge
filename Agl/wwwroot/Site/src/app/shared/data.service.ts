import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { CatResponse } from '../pet/cat.response.model';
@Injectable(
    {
        providedIn: 'root'
    }
)
export class DataService{
    constructor(private httpClient:HttpClient)
    {
    }
    getAllCat()
    {
        return this.httpClient.get<CatResponse[]>(environment.apiUrl+"pet");
    }
}
