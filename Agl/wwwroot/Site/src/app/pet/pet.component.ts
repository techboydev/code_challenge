import { OnInit, Component } from '@angular/core';
import { DataService } from '../shared/data.service';
import { Pet } from './pet.model';
import { HttpErrorResponse } from '@angular/common/http';
import { CatResponse } from './cat.response.model';

@Component({
    selector:'app-pet',
    templateUrl: './pet.component.html',
    styleUrls: ['./pet.component.scss'],
})
export class PetComponent implements OnInit
{
    femalePetCollection:Pet[]=[];
    malePetCollection:Pet[]=[];
    errorResponse:HttpErrorResponse;
    constructor(private dataProvider:DataService)
    {
    }
    ngOnInit()
    {
        
        this.dataProvider.getAllCat().subscribe((pets:CatResponse[])=>{
            //filter the response to get cats by male and female.
            var male=pets.filter((r:CatResponse)=>{
                return r.gender.toLowerCase()=='male';
            });
            var female=pets.filter((r:CatResponse)=>{
                return r.gender.toLowerCase()==='female';
            });
            //set the values for corresponding collections.
            this.malePetCollection=male[0].cats;
            this.femalePetCollection=female[0].cats;
            
        },
        (error:HttpErrorResponse)=>{
            this.errorResponse=error;
        });
        
    }
}