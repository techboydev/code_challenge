import {
  async,
  ComponentFixture,
  TestBed,
  inject,
  fakeAsync,
  tick
} from "@angular/core/testing";
import { DataService } from "../shared/data.service";
import {
  HttpClientTestingModule,
  HttpTestingController
} from "@angular/common/http/testing";
import { PetComponent } from "./pet.component";
import { CustomMaterialModule } from "../shared/custom.material.module";
import { of, throwError } from "rxjs";
import { Pet } from "./pet.model";
import { HttpErrorResponse } from "@angular/common/http";
import { CatResponse } from "./cat.response.model";

describe("Pet Component Testing", () => {
  //declare dependencies to be used for each test
  let mockedCatResponse: CatResponse[];

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [PetComponent],
      imports: [HttpClientTestingModule, CustomMaterialModule],
      providers: [DataService]
    });

    mockedCatResponse = [
      {
        gender: "female",
        cats: [{ name: "Jes", type: "Fish" }]
      },
      {
        gender: "male",
        cats: [{ name: "Nemo", type: "Cat" }]
      }
    ];
  });

  it("Pet Component Created", () => {
    let fixture = TestBed.createComponent(PetComponent);
    let app = fixture.debugElement.componentInstance;
    expect(app).toBeTruthy();
  });

  it("Pet Component OnInit() Test", () => {
    let fixture = TestBed.createComponent(PetComponent);
    let app = fixture.debugElement.componentInstance;

    //spy on getPet event or method, giving back the mock response
    let dataService = fixture.debugElement.injector.get(DataService);
    let spy = spyOn(dataService, "getAllCat").and.returnValue(
      of(mockedCatResponse)
    );
    app.ngOnInit();
    expect(app.femalePetCollection).toContain({ name: "Jes", type: "Fish" });
  });

  it("Loading all cats using service", fakeAsync(() => {
    let fixture = TestBed.createComponent(PetComponent);
    let dataService = fixture.debugElement.injector.get(DataService);
    let spy = spyOn(dataService, "getAllCat").and.returnValue(
      of(mockedCatResponse)
    );
    let response;
    dataService.getAllCat().subscribe(pets => (response = pets));
    tick();
    expect(response).toEqual(mockedCatResponse);
  }));

  it("Non response from server", fakeAsync(() => {
    let fixture = TestBed.createComponent(PetComponent);
    let app = fixture.debugElement.componentInstance;
    let errorMock = {
      status: 0,
      error: {
        message: "Unable to reach the back end"
      }
    };
    //spy on getPet event or method, giving back the mock response
    let dataService = fixture.debugElement.injector.get(DataService);
    let spy = spyOn(dataService, "getAllCat").and.returnValue(
      throwError(errorMock)
    );
    
    let response;
    dataService
      .getAllCat()
      .subscribe(
        pets => (response = pets),
        error => (app.errorResponse = error)
      );
    tick();
    expect(app.errorResponse).toEqual(errorMock);
  }));
});