import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { FormsModule } from '@angular/forms';
import { HttpClientModule, HttpClient } from '@angular/common/http';
import { PetComponent } from './pet/pet.component';
import { DataService } from './shared/data.service';
import { CustomMaterialModule } from './shared/custom.material.module';

@NgModule({
  declarations: 
  [
    AppComponent,
    PetComponent
  ],
  imports: 
  [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    HttpClientModule,
    CustomMaterialModule,
  ],
  providers: 
  [
    HttpClientModule,
    DataService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
