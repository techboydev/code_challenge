﻿using Agl.Models.Pets;
using Microsoft.AspNetCore.Mvc;
using Agl.Models.Interface;
using System.Linq;
using System.Net.Mime;

namespace Agl.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class PetController : ControllerBase
    {
        private IPetService petService;
        /// <summary>
        /// Pet Controller Constructor
        /// </summary>
        /// <param name="_petService">Instance implement IPetService</param>
        public PetController(IPetService _petService) 
        {
           petService = _petService;
           
        }
        /// <summary>
        /// Default get method return a collection of Cat Response.
        /// </summary>
        /// <returns>CatResponse array</returns>
        [HttpGet]
        public CatResponse[] Get()
        {
            return petService.GetAllCats();
        }

    }
}
