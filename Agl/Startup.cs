﻿using System;
using Agl.Models.DataProivder;
using Agl.Models.Interface;
using Agl.Models.Pets;
using Agl.Models.Services;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.HttpsPolicy;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;

namespace Agl
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddMvc().SetCompatibilityVersion(CompatibilityVersion.Version_2_1);
            services.AddCors(options=> {
                options.AddPolicy("Development", builder => builder.WithOrigins("http://localhost:4200").AllowAnyMethod().AllowAnyHeader());
                //adding extra cor policy for other environment
               // options.AddPolicy("testing", builder => builder.WithOrigins("http://localhost:4200").AllowAnyMethod().AllowAnyHeader());

            });
            //declaring dependencies service, so container can return corresponding services,
            ///when it gets called.
            services.AddHttpClient();
            services.AddScoped<IDataProvider, DataProvider>();
            services.AddScoped<IPetService, PetService>();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                app.UseHsts();
            }
            app.UseCors("Development");
            app.UseHttpsRedirection();
            app.UseMvc();
        }
    }
}
