﻿using System;
using System.Collections.Generic;
using System.Text;
using Agl;
using Microsoft.Data.Sqlite;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc.Testing;
using Microsoft.Extensions.DependencyInjection;

namespace Agl.Integration.Test
{
    public class CustomWebApplicationFactory<TStartup> : WebApplicationFactory<Startup> // IDisposable where TStartup : class
    {

        protected override void ConfigureWebHost(IWebHostBuilder builder)
        {
         
            builder.ConfigureServices(services =>
            {
                // Build the service provider
                var sp = services.BuildServiceProvider();

                using (var scope = sp.CreateScope())
                {
                    var scopedServices = scope.ServiceProvider;
                   
                }
            });
        }
    }
}
