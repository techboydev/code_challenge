using System;
using Xunit;
using Microsoft.Extensions.DependencyInjection;
using Agl.Models.Pets;
using Agl.Controllers;
using System.Net.Http;
using System.Threading.Tasks;
using Newtonsoft.Json;
using System.Linq;

namespace Agl.Integration.Test
{
    public class PetControllerTest: IClassFixture<CustomWebApplicationFactory<Startup>>
    {
        private readonly CustomWebApplicationFactory<Startup> factory;
        private readonly HttpClient client;
        public PetControllerTest(CustomWebApplicationFactory<Startup> _factory)
        {
            factory = _factory;
            client = factory.CreateClient();
        }
        [Fact]
        public async Task ValidControllerEndPoint_ShouldReturnSuccess()
        {
            //arrange
            var endPoint = "/api/pet";
           //act
            var res = await client.GetAsync(endPoint);
            //assert
            Assert.Equal("OK", res.StatusCode.ToString());
            
        }
        [Theory]
        [InlineData("Male")]
        [InlineData("Female")]
        public async Task ReadDataService_ShouldReturnResponse(string gender)
        {
            var res = await client.GetAsync("/api/pet");
            //act
            string content = await res.Content.ReadAsStringAsync();
            var pets=JsonConvert.DeserializeObject<CatResponse[]>(content);
            
            //assert
            Assert.True(pets.Count()>0);
            Assert.Contains(pets, x => string.Compare(gender, x.Gender, true) == 0);
        }
      
    }
}
