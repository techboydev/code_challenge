## Agl

Sample front-end angular and back-end .net core to consume a data from http://agl-developer-test.azurewebsites.net/people.json


## Development server Set Up

##----------Steps to build and lauch the back-end(.NETCORE)----------

#Step 1 - Clone from the repo

Clone the code from the given repo, it contains both front and back ends

#Required software to run

- Visual studio Community 2017
- .Netcore 2.1 packages to be installed

#Step 2 - Build and run the backend using visual studio

- After you have installed the required software, open the solution in visual studio and buil the project name "Agl".
- ***Note- you may have issue when try to compile the project since it contains front-end(angular site) in `Agl solution -> Agl Project -> wwwroot-> Site`
if that is the case. At terminal or cmd, change path to `Agl solution -> Agl Project -> wwwroot-> Site` and run `npm install` before compiling it again.

- There is also a test project included named "Agl.Integration.Test" for testing purpose.

- The default server is build using http protocol, if you require to use ssl (https), please make change to the lauch "launchSettings.json" under "Agl > Properties" folder or accessing 'Project Properties' from visual studio.

- If you try to use the https protocol, you will be prompt to trust the certificate for the first time that it runs.

#Step 3- Run test project(optional)

- In order to execute the test project, you can use visual studio Run test feature or exec from cmd or terminal.

Access to this path `Agl solution root folder`

- Run "dotnet test" for integration test projects at root folder of the solution.

##-----Steps to build and lauch the front-end(Angular Version 2)------

#Require package

- NodeJs latest version to be installed
- Angular CLI to be installed.

#Step 1 - Clone from the repo

Accessing the source code under `Agl solution -> Agl Project -> wwwroot-> Site` to find the root of front end.

You may like to open it using different editor of your choice if that increases the readibility of the code.

#Step 2 - Install angular dependecies for the project before running it.

- Using cmd or terminal at `Agl solution -> Agl Project -> wwwroot-> Site`

- Run `npm install` to install all required packages for angular. Once it is done.

- Run `ng serve --open` so angular can build the packages and lauch the site. Note that --open is optional, it just opens the browser when it is done, you can omit it.

# Step 3 - Run unit test with test coverage report.

The front-end does include unit test built with karma and jasmine. In order to run it.

- At a different cmd or terminal, accessing to `Agl solution -> Agl Project -> wwwroot-> Site`

- Run `ng test --code-coverage` for tests to be run and report to be generated.

- You will notice the completion report from cmd as well as you can access the report under `Agl solution -> Agl Project -> wwwroot-> Site->coverage`



## Common Issues

- For some reason, the default port (4200) is not available for angular to be lauched, and you switch to a different port other than 4200, it also requires to change the backend start up file to the new port in order for supporting the cross origin request(Cors) to work. Please see more detail below

## The application support Cors(Cross origin request) communication between front and back ends.

- If you happen to change the front site url which is different from (default-http://localhost:4200), then it needs to be configured in the start up in order for Cor policy to work.
- Please see more details on https://stackoverflow.com/questions/44379560/how-to-enable-cors-in-asp-net-core-webapi
